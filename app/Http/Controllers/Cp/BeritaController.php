<?php

namespace App\Http\Controllers\Cp;

use Illuminate\Http\Request;

use App\Berita;
use Storage;
use Validator;
use App\Http\Controllers\Controller;

class BeritaController extends Controller
{
    public function __construct(Request $req) {
        $this->request = $req;
    }

    public function store(Request $request){
        $input = $request->all();

        $this->validate($request,[
            'judul_berita' => 'required|max:100|String',
            'penulis_berita' => 'required|max:100|String',
            'tanggal_berita' => 'required',
            'gambar_berita' => 'required|image|max:4000',
            'isi_berita' => 'required|String',
        ]);

        //picture
        if($request->hasFile('gambar_berita')){
            $gambar_berita = $request->file('gambar_berita');
            $ext = $gambar_berita->getClientOriginalExtension();
            if($request->file('gambar_berita')->isValid()){
                $picture_name = date('YmdHis'). ".$ext";
                $upload_path = 'public/imageupload';
                $request->file('gambar_berita')->move($upload_path, $picture_name);
                $input['gambar_berita'] = $picture_name;
            }
        }

        $berita = new Berita($input);
        
                if ($berita->save()) {
                    return redirect('cp')->with('sukses','Data Berhasil diinput');
                }
            }
            public function create_berita(){
                return view('cp/berita/create_berita');
            }
        

}
